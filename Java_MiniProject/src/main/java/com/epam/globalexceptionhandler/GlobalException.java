package com.epam.globalexceptionhandler;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class GlobalException {
	
	String timeStamp;
	String status;
	String error;
	String path;
	
}
