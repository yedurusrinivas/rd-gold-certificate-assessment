package com.epam.globalexceptionhandler;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import java.util.List;
import com.epam.exceptions.AssociateException;

@RestControllerAdvice
@Component

public class ResponseGlobalException {
	
	
	@ExceptionHandler(value = AssociateException.class)
	public GlobalException handleAssociate(AssociateException exception, WebRequest request) {

		return new GlobalException(new Date().toString(), HttpStatus.NOT_FOUND.name(), exception.getMessage(),
				request.getDescription(false));
	}

	
	@ExceptionHandler(DataIntegrityViolationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public GlobalException handleIntegrityVoilations(DataIntegrityViolationException exception, WebRequest request) {
		return new GlobalException(new Date().toString(), HttpStatus.BAD_REQUEST.name(),
				"Please Check that Associate should map to the Available batches only ...!",
				request.getDescription(false));
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public GlobalException handlemethodArgument(MethodArgumentNotValidException exception, WebRequest request) {

		List<String> inputerrors = new ArrayList<>();
		exception.getAllErrors().forEach(error -> {
			inputerrors.add(error.getDefaultMessage());
		});
		return new GlobalException(new Date().toString(), HttpStatus.BAD_REQUEST.name(), inputerrors.toString(),
				request.getDescription(false));
	}

}
