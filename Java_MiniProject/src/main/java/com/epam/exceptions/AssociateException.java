package com.epam.exceptions;

public class AssociateException extends Exception {
	public AssociateException(String message)
	{
		super(message);
	}
}
