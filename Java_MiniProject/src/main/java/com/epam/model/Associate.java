package com.epam.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Associates")
@Getter
@Setter
@NoArgsConstructor
public class Associate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String email;
    private String gender;
    private String college;
    private String status;

    @ManyToOne
    @JoinColumn(name = "batch_id")
    private Batch batch;
    public Associate(String name, String email, String gender, String college, String status, Batch batch) {
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.college = college;
        this.status = status;
        this.batch = batch;
    }
}