package com.epam.dto;
import jakarta.validation.constraints.NotBlank;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.epam.model.Batch;
@NoArgsConstructor
@Getter
@Setter
public class AssociateDTO {
	public int id;
	@NotBlank(message ="Name must be valid ")
    public String name;
	public String email;
	public String gender;
	public String college;
	public String status;
	public Batch batch;
    public AssociateDTO(String name, String email, String gender, String college, String status, Batch batch) {
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.college = college;
        this.status = status;
        this.batch = batch;
    }
}
