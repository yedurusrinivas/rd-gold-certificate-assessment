package com.epam.dto;

import java.util.Date;


import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@NoArgsConstructor
@Getter
@Setter
public class BatchDTO {
	 public int id;
	 @NotBlank(message = "Name of batch should be valid")
	 public String name;
	 @Enumerated(EnumType.STRING)
	 public Practice practice;
	 public enum Practice {
	        JAVA,
	        TESTING,
	        DEP,
	        DOTNET
	    }
	 
	    @NotBlank(message = "Start date must be valid")
	    public Date startDate;
	    @NotBlank(message = "End date must be valid ")
	    public Date endDate;
	   
}
