package com.epam.repository;

import org.springframework.data.repository.CrudRepository;

import com.epam.model.Batch;

public interface BatchRepository extends CrudRepository<Batch, Integer> {

}
