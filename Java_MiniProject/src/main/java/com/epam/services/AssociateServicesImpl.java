package com.epam.services;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.modelmapper.TypeToken;
import com.epam.dto.AssociateDTO;
import com.epam.exceptions.AssociateException;
import com.epam.model.Associate;
import com.epam.repository.AssociateRepository;

@Component
public class AssociateServicesImpl implements AssociateServices {
	
	@Autowired
	AssociateRepository associateRepository;
	
	@Autowired
	ModelMapper mapper;
	@Autowired
    TypeToken<List<AssociateDTO>> associateTypeToken;
	
	@Override
	public AssociateDTO save(AssociateDTO associateDto) {
		associateRepository.save(mapper.map(associateDto, Associate.class));
		return associateDto;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AssociateDTO> getByGender(String gender){
		return associateRepository.findAllByGender(gender).stream().map(associate -> mapper.map(associate, AssociateDTO.class)).toList();
               
	}

	@Override
	public AssociateDTO updateAssociate(AssociateDTO associateDto, int id) throws AssociateException {
		return mapper.map(associateRepository.findById(id).map(associate -> {

			associateDto.setId(id);

			 associateRepository.save(mapper.map(associateDto, Associate.class));

			 return associate;

			 }).orElseThrow(() -> new AssociateException("Associate not found....")), AssociateDTO.class);
	}

	@Override
	public void DeleteAssociate(int id)  {
			associateRepository.deleteById(id);
	}
}
