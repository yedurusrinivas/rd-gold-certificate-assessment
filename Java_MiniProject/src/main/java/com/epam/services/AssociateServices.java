package com.epam.services;

import java.util.List;

import com.epam.dto.AssociateDTO;
import com.epam.exceptions.AssociateException;

public interface AssociateServices {
		
	public AssociateDTO save(AssociateDTO associateDto);
	public List<AssociateDTO> getByGender(String gender);
	public AssociateDTO updateAssociate(AssociateDTO associateDto,int id) throws AssociateException;
	public void DeleteAssociate(int id)  ;
}
