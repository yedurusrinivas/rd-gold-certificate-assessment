package com.epam.services;


import com.epam.dto.BatchDTO;

public interface BatchServices {
	
	public BatchDTO save(BatchDTO batchdto);
}
