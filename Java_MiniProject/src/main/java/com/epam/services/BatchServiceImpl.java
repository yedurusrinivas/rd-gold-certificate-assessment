package com.epam.services;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.model.Batch;
import com.epam.dto.BatchDTO;
import com.epam.repository.BatchRepository;
@Component
public class BatchServiceImpl implements BatchServices {
	
	@Autowired
	BatchRepository batchRepository;
	@Autowired
	ModelMapper mapper;
	@Override
	public BatchDTO save(BatchDTO batchdto) {
		batchRepository.save(mapper.map(batchdto, Batch.class));
		return batchdto;
	}

}
