package com.epam.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.AssociateDTO;
import com.epam.exceptions.AssociateException;
import com.epam.services.AssociateServicesImpl;

import jakarta.validation.Valid;

@RestController
@RequestMapping("rd/associates")
public class AssociateController {
		
	@Autowired
	AssociateServicesImpl associateServiceImpl;
	
	@PostMapping
	public ResponseEntity<Object> save(@RequestBody AssociateDTO associateDto)
	{
		return ResponseEntity.status(HttpStatus.CREATED).body(associateServiceImpl.save(associateDto));
	}
	
	@GetMapping("/{gender}")
	public ResponseEntity<List<AssociateDTO>> getByGender(@PathVariable String gender) 
	{
		return ResponseEntity.status(HttpStatus.OK).body(associateServiceImpl.getByGender(gender));
	}
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteAssociate(@PathVariable @Valid int id)  {
		associateServiceImpl.DeleteAssociate(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } 
	@PutMapping("/{id}")
	public ResponseEntity<Object> updateAssociate(@RequestBody AssociateDTO associateDto,@PathVariable int id) throws AssociateException
	{
		return ResponseEntity.status(HttpStatus.CREATED).body(associateServiceImpl.updateAssociate(associateDto, id));
	}
	
}
 
