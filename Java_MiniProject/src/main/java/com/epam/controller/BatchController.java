package com.epam.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.BatchDTO;
import com.epam.services.BatchServiceImpl;

@RestController
@RequestMapping("rd/batch")
public class BatchController {

		@Autowired
		BatchServiceImpl batchServiceImpl;
		
		@PostMapping
		public ResponseEntity<Object> save(@RequestBody BatchDTO batchdto)
		{
			return ResponseEntity.status(HttpStatus.CREATED).body(batchServiceImpl.save(batchdto));
		}
		
}
