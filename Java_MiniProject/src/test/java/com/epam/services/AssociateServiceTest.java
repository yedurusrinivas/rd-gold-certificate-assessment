package com.epam.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import com.epam.dto.AssociateDTO;
import com.epam.exceptions.AssociateException;
import com.epam.model.Associate;
import com.epam.model.Batch;
import com.epam.repository.AssociateRepository;

@ExtendWith(MockitoExtension.class)
public class AssociateServiceTest {
	
	 	@Mock
	    AssociateRepository associateRepository;

	    @Mock
	    ModelMapper modelMapper;

	    @Mock
	    TypeToken<List<AssociateDTO>> associateTypeToken;

	    @InjectMocks
	    AssociateServicesImpl associateServiceImpl;
	    static Associate associate;
	    static AssociateDTO associateDto;
	    @BeforeAll
	    static void setUp() {
	    	associate = new Associate("tom","tom@gmail.com","male","vignan","active",new Batch());
	       associateDto = new AssociateDTO("tom","tom@gmail.com","male","vignan","active",new Batch());
	    }

	    @Test
	    void createAssociateTest() {

	        Mockito.when(associateRepository.save(associate)).thenReturn(associate);
	        Mockito.when(modelMapper.map(associateDto,Associate.class)).thenReturn(associate);

	        assertEquals(associateDto,associateServiceImpl.save(associateDto));
	    }

	    @Test
	    void deleteAssociateTest() throws AssociateException {
	        Mockito.doNothing().when(associateRepository).deleteById(1);
	        associateServiceImpl.DeleteAssociate(1);
	    }

	    @Test
	    void updateTest() throws AssociateException {
	    	Mockito.when(associateRepository.findById(2)).thenReturn(Optional.of(associate));
	        Mockito.when(modelMapper.map(associateDto,Associate.class)).thenReturn(associate);
	        Mockito.when(associateRepository.save(Mockito.any(Associate.class))).thenReturn(associate);
	        associateServiceImpl.updateAssociate(associateDto,2);
	        Mockito.verify(associateRepository).save(associate);
	    }
	    @Test
	    void getByGenderTest()
	    {
	    	List<Associate> associatelist=new ArrayList<>();
	    	associatelist.add(associate);
	        List<AssociateDTO> associatedtolist=new ArrayList<>(Arrays.asList(associateDto));
	       Mockito.when(associateRepository.findAllByGender("m")).thenReturn(associatelist);
	       Mockito.when(modelMapper.map(associate, AssociateDTO.class)).thenReturn(associateDto);
	        assertEquals(associatedtolist, associateServiceImpl.getByGender("m"));
	    }
	    @Test
	    void updateExceptionTest() 
	    {
	    	Mockito.when(associateRepository.findById(1)).thenReturn(Optional.empty());
	    	assertThrows(AssociateException.class, ()->associateServiceImpl.updateAssociate(associateDto, 1));
	    }
	    
}
