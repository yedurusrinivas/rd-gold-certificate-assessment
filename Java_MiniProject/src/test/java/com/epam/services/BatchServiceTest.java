package com.epam.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.dto.BatchDTO;
import com.epam.model.Batch;
import com.epam.repository.BatchRepository;

@ExtendWith(MockitoExtension.class)
public class BatchServiceTest {
	@Mock
	BatchRepository batchRepository;
	@Mock
	ModelMapper modelMapper;
	
	@InjectMocks
    BatchServiceImpl batchServiceImpl;
	
	static Batch batch = new Batch();
	static BatchDTO batchdto = new BatchDTO();
	
	@Test
	void creationbatchTest()
	{
		Mockito.when(batchRepository.save(batch)).thenReturn(batch);
        Mockito.when(modelMapper.map(batchdto,Batch.class)).thenReturn(batch);

        assertEquals(batchdto,batchServiceImpl.save(batchdto));
	}
	
}
