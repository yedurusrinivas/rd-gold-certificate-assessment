package com.epam.Controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.controller.BatchController;
import com.epam.dto.AssociateDTO;
import com.epam.dto.BatchDTO;
import com.epam.services.BatchServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@WebMvcTest(BatchController.class)
@RequestMapping("rd/batch")
public class BatchControllerTest {
	
	@MockBean
	private BatchServiceImpl batchServiceImpl;
	@Autowired
	private MockMvc mockmvc;
	AssociateDTO associateDto;
	BatchDTO batchDto;
	@BeforeEach
	void batchDetails() {
	 associateDto = new AssociateDTO();
	 associateDto.setId(1);
	 associateDto.setName("srinivas");
	 associateDto.setEmail("srinvas.com");
	 associateDto.setId(1);
	 associateDto.setGender("f");
	 associateDto.setStatus("active");
	 associateDto.setCollege("RVR and JC");
	 List<AssociateDTO> associateDtos = new ArrayList<>();
	 associateDtos.add(associateDto);
	 batchDto = new BatchDTO();
	 batchDto.setId(1);
	 batchDto.setName("Java");
	 batchDto.setPractice(BatchDTO.Practice.JAVA);
	 batchDto.setEndDate(new Date());
	 batchDto.setStartDate(new Date());
	 }
	
	
	@Test
	void testAddBatch() throws JsonProcessingException, Exception {
	Mockito.when(batchServiceImpl.save(batchDto)).thenReturn(batchDto);
	mockmvc.perform(post("/rd/batch").contentType(MediaType.APPLICATION_JSON)
	.content(new ObjectMapper().registerModule(new JavaTimeModule()).writeValueAsString(batchDto))).andExpect(status().isCreated());
	}
	
	
	@Test
	 void testAddBatchWithMethodArgumentNotValidException() throws JsonProcessingException, Exception {
		BatchDTO batchDto = new BatchDTO();
		when(batchServiceImpl.save(batchDto)).thenThrow(RuntimeException.class);
		mockmvc.perform(post("/rd/batch").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(batchDto))).andExpect(status().isCreated());
		}
}
