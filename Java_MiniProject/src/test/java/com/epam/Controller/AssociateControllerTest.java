package com.epam.Controller;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.controller.AssociateController;
import com.epam.dto.AssociateDTO;
import com.epam.services.AssociateServicesImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;



@WebMvcTest(AssociateController.class)

public class AssociateControllerTest {
	@MockBean
	 private AssociateServicesImpl associateService;
	@Autowired
	private MockMvc mockMvc;
	 AssociateDTO associateDto;
	 List<AssociateDTO> associateDtos = new ArrayList<>();
	@BeforeEach
	void associateDetails() {
		 associateDto = new AssociateDTO();
		associateDto.setId(1);
		associateDto.setName("srinivas");
		 associateDto.setEmail("srinivas@gmail.com");
		associateDto.setId(1);
		associateDto.setGender("Male");
		 associateDto.setStatus("active");
		associateDto.setCollege("vignan");
		associateDtos.add(associateDto);;
	}
	@Test
	void testGetAssociatesWithGender() throws Exception {
		when(associateService.getByGender("male")).thenReturn(associateDtos);
		mockMvc.perform(get("/rd/associates/{gender}",1).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	@Test
	void testAddAssociate() throws JsonProcessingException, Exception {
		 when(associateService.save(Mockito.any(AssociateDTO.class))).thenReturn(associateDto);
		 mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON)
		 .content(new ObjectMapper().writeValueAsString(associateDto))).andExpect(status().isCreated());
	 }
	@Test
	void testDeleteAssociate() throws JsonProcessingException, Exception {
		 doNothing().when(associateService).DeleteAssociate(1);
		mockMvc.perform(delete("/rd/associates/{associateId}", 1).contentType(MediaType.APPLICATION_JSON))
		 .andExpect(status().isNoContent());
	}
	@Test
	void testUpdateAssociate() throws JsonProcessingException, Exception {
	 when(associateService.updateAssociate( associateDto,1)).thenReturn(associateDto);
	 mockMvc.perform(put("/rd/associates/{associateId}", 1).contentType(MediaType.APPLICATION_JSON)
	 .content(new ObjectMapper().writeValueAsString(associateDto))).andExpect(status().isCreated());
	 }
}

